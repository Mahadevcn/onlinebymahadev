from selenium import webdriver
from selenium.webdriver import ActionChains

driver = webdriver.Chrome()
driver.get("https://www.annauniv.edu/department/index.php")
driver.maximize_window()
driver.implicitly_wait(10)

action = ActionChains(driver)
menu = driver.find_element_by_xpath("//*[@id=\"link3\"]/strong")
second = driver.find_element_by_xpath("//*[@id=\"menuItemHilite32\"]")
action.move_to_element(menu).move_to_element(second).click().perform()

print(driver.title)
driver.quit()

