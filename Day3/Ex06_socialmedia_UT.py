import unittest
from selenium import webdriver

class MyTestCase(unittest.TestCase):
    def testHref(self):
        driver = webdriver.Chrome()
        driver.maximize_window()
        driver.implicitly_wait(10)

        driver.get('http://agiletestingalliance.org/')
        lst = driver.find_elements_by_xpath("//*[@id=\"custom_html-10\"]/div/ul/li/a")

        for webel in lst:
            address = webel.get_attribute("href")
            print(address)

            driver.quit()

#     def test_something(self):
#         self.assertEqual(True, False)
#
#
# if __name__ == '__main__':
#     unittest.main()
