from Day3.Classes.PersonClass import Person


class Student (Person):

    def __init__(self, name, age , std):
        super().__init__(name, age)
        self.std = std

    def greetings(self):
        print('Greeting from :', self.name)

# method overriding
    def sayHello(xyz):
        print('I am a student of standard', xyz.std, 'And saying hello to all')


st1 = Student('Rajesh',13, 'VIII')
print(st1.name, st1.age, st1.std)
st1.sayHello()
st1.greetings()

p1 = Person('Abhishek', 29)
p1.sayHello()
