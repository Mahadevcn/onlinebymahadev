import unittest

from Day3.Classes.CalculatorClass import Calc
class MyTestCase(unittest.TestCase):
    #MyTestCase is a subclass of unittest.TestCase
    def test_validateAdd (self):
        print('Inside test_validateAdd() method')
        expres1 =30
        res1= Calc.add(self,10,20)
        self.assertEqual(expres1,res1, "Failed due to mismatching ")

    def test_mult(self):
        print('Inside test_mult() method')
        self.assertEqual(200, Calc.mul(self,10, 20))
#
# if __name__ == '__main__':
#     unittest.main()
