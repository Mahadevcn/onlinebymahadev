class Person:

    def __init__(self,name,age):
        self.name = name
        self.age = age

    def sayHello (self):
        print("Helo everyone My name is :",self.name)


p1 = Person('Ashish', 24)
print(p1.name, p1.age)
p2 = Person ('Virat', 25)
print(p2.name, p2.age)

p3 = Person('Akash',30)
print(p3.name, p3.age)

p1.sayHello()
p2.sayHello()
p3.sayHello()
