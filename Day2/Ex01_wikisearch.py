import time

from selenium import webdriver

driver = webdriver.Chrome()
driver.maximize_window()

driver.get('https://www.wikipedia.org/')
driver.save_screenshot('../Screenshots/wikipage1.png')


pageTitle = driver.title
print('Title of page 1 :',pageTitle)

# //*[@id="js-link-box-en"]/strong

# weenglishlink = driver.find_element_by_xpath("//*[@id=\"js-link-box-en\"]/strong")
weenglishlink = driver.find_element_by_id("js-link-box-en")
weenglishlink.click()
print('Title of page 2:', driver.title)

driver.save_screenshot('../Screenshots/wikipage2.png')

# //*[@id="searchInput"]

#
# webox = driver.find_element_by_xpath("//*[@id=\"searchInput\"]")
# webox = driver.find_element_by_name('search')
webox = driver.find_element_by_id('searchInput')
print(type(webox))

webox.send_keys('Selenium')
# //*[@id="searchButton"]
searchbox = driver.find_element_by_xpath("//*[@id=\"searchButton\"]").click()
# searchbox.click()

print('Title of page 3:', driver.title)

driver.save_screenshot('../Screenshots/wikipage3.png')

assert driver.title == 'Selenium - Wikipedia'
driver.back()
print('Title of the page after moving back from page 3:', driver.title)

driver.forward()
print('Title of the page after moving forward from page 2:', driver.title)


time.sleep(2)
driver.close()