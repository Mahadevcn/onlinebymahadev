from selenium import webdriver

driver = webdriver.Chrome()
driver.maximize_window()

driver.get('http://agiletestingalliance.org/')

# //*[@id="custom_html-10"]/div/ul/li[1]/a
# //*[@id="custom_html-10"]/div/ul/li[2]/a
#
# //*[@id="custom_html-10"]/div/ul/li/a for all (7)

lst = driver.find_elements_by_xpath("//*[@id=\"custom_html-10\"]/div/ul/li/a")

for webel in lst:
    address = webel.get_attribute("href")
    print(address)