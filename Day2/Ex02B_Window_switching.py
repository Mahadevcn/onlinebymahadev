from selenium import webdriver


driver = webdriver.Chrome()
driver.maximize_window()
driver.implicitly_wait(10)

driver.get("https://www.ataevents.org/")

# //*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[1]/div/div/div/div/div/figure/a/img
link1 = driver.find_element_by_xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[1]/div/div/div/div/div/figure/a/img")
link1.click()

# //*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[2]/div/div/div/div/div/figure/a/img
link2 = driver.find_element_by_xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[2]/div/div/div/div/div/figure/a/img")
link2.click()

# driver.close()
# driver.quit()
handle = driver.window_handles
for winhe in handle:
    print(winhe)
    driver.switch_to.window(winhe)
    print(driver.title)
driver.quit()