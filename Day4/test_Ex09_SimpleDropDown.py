# from select import select
import time

from selenium import webdriver
from selenium.webdriver.support.select import Select


def setup():
    print('Inside Setup() method ')
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print('Inside teardown() method')
    time.sleep(3)
    # driver.quit()

def test_verify_dropdwon():
    print('Inside test_drop_Down method')
    driver.get("file:///C:/Users/Anyone/PycharmProjects/MyfirstPycharm%20project/resource/data/dropdown.html")
    # / html / body / form / select
    dd = driver.find_element_by_xpath("/ html / body / form / select")
    selectObject = Select(dd)

    lstOptions = selectObject.options
    for opt in lstOptions:
        print(opt.text)
    time.sleep(3)
    # selectObject.select_by_visible_text("CP-AAT")
    # selectObject.select_by_value("3")
    # selectObject.select_by_index(2)

def test1_verify_dropdwonbytext():
    print('Inside test_drop_Downbytext method')
    driver.get("file:///C:/Users/Anyone/PycharmProjects/MyfirstPycharm%20project/resource/data/dropdown.html")
    dd = driver.find_element_by_xpath("/html/body/form/select")
    selectobject = Select(dd)
    selectobject.select_by_visible_text("CP-AAT")

def test2_verify_dropdwonbytext():
    print('Inside test_drop_Downbytext method')
    driver.get("file:///C:/Users/Anyone/PycharmProjects/MyfirstPycharm%20project/resource/data/dropdown.html")
    dd = driver.find_element_by_xpath("/html/body/form/select")
    selectobject = Select(dd)
    selectobject.select_by_index(3)
