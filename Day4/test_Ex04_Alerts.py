from selenium import webdriver


def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print('inside teardown() method')
    # driver.quit()

def test_validate_JSAlert():
    print('Inside test_validate_JSAlert()')
    driver.get('https://the-internet.herokuapp.com/javascript_alerts')
    # // *[ @ id = "content"] / div / ul / li[1] / button
    first = driver.find_element_by_xpath("// *[ @ id = 'content'] / div / ul / li[1] / button")
    first.click()

    alert1 = driver.switch_to.alert

    alert1.accept()
    expResult = 'You successfully clicked an alert'
    # // *[ @ id = "result"]

    actualres = driver.find_element_by_xpath("// *[ @ id = 'result']").text

    if(expResult == actualres):
        print('You have successfully clicked an alert')

def test1_validate_JSconfirm():
    print('inside test1_validate_JSConfirm() method')
    driver.get("https://the-internet.herokuapp.com/javascript_alerts")
    second = driver.find_element_by_xpath('//*[@id=\"content\"]/div/ul/li[2]/button').click()

    alert2 = driver.switch_to.alert
    alert2.accept()
    expectedres = 'You clicked: Ok'
    actualres = driver.find_element_by_xpath('//*[@id=\"result\"]').text

    assert expectedres == actualres
    print('you clicked : ok')

def test2_validate_JSprompt():
    print('inside test2_validate_JSprompt() method')
    driver.get('https://the-internet.herokuapp.com/javascript_alerts')
    third = driver.find_element_by_xpath('//*[@id=\"content\"]/div/ul/li[3]/button').click()
    alter3 = driver.switch_to.alert
    alter3.send_keys('Ammu is a drama queen, but Nirupama is not')
    alter3.accept()
    expectedres = 'You entered: Ammu is a drama queen, but Nirupama is not'
    actualres = driver.find_element_by_xpath('//*[@id=\"result\"]')

    # assert expectedres == actualres





# //*[@id="content"]/div/ul/li[2]/button
#
# You clicked: Cancel
# //*[@id="result"]

# You clicked: Ok

# def test_validate_JSAlert2():
#     print('Inside test_validate_JSAlert2()')
#     driver.get('https://the-internet.herokuapp.com/javascript_alerts')
#     # // *[ @ id = "content"] / div / ul / li[1] / button
#     first = driver.find_element_by_xpath("//*[@id='content']/div/ul/li[2]/button")
#     first.click()
#
#     alert1 = driver.switch_to.alert
#
#     alert1.dismiss()
#     expResult = 'You clicked: Cancel'
#     # // *[ @ id = "result"]
#
#     actualres = driver.find_element_by_xpath("//*[@id='result']").text
#     assert expResult == actualres
#
#
#     # if(expResult == actualres):
#     #     print('You clicked: Cancel')
#
#     first1 = driver.find_element_by_xpath("//*[@id='content']/div/ul/li[2]/button")
#     first1.click()
#
#     alert = driver.switch_to.alert
#
#     alert.accept()
#     expResult1 = 'You clicked: Ok'
#     # // *[ @ id = "result"]
#
#     actualres1 = driver.find_element_by_xpath("//*[@id='result']").text
#
#     if (expResult1 == actualres1):
#         print('You clicked: Ok')
#

