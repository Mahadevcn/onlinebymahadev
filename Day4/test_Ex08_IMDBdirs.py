import time

import pytest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def setup():
    print('Inside the setup() method')
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print('inside teardown() method')
    time.sleep(3)
    driver.quit()

def dataGenerator():
    lst = [['Uri', 'Aditya Dhar'], ['Raazi', 'Meghna Gulzar']]
    return lst

@pytest.mark.parametrize('data',dataGenerator())

def test_searchdir(data):
    movie = data[0]
    expDir = data[1]

    driver.get('https://www.imdb.com/')
    search = driver.find_element_by_xpath("//*[@id=\"suggestion-search\"]")
    search.send_keys(movie)
    search.send_keys(Keys.ENTER)

    movielnk = driver.find_element_by_xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")
    movielnk.click()

    webeldir = driver.find_element_by_xpath("//*[@id=\"title-overview-widget\"]/div[2]/div[1]/div[2]/a")
    # // *[ @ id = "title-overview-widget"] / div[2] / div[1] / div[2] / h4
    actualdir = webeldir.text
    # print(actualdir)
    Dirfound = False
    if(expDir == actualdir):
        Dirfound = True
        print('Director found ::::', expDir)
    # // *[ @class ='inline' and contains(text(), 'Director')]
    #
    # for dir in lstdir:
    #     dirname = dir.text
    #     print(dirname)
    # expdir = "Aditya Dhar"
    # # Dirfound = False
    # if(expdir == lstdir.text):
    #     # Dirfound = True
    #     print('Found the director ::::', expdir)
        # if (Expdir in dirname):
        #     Dirfound = True
        #     print('Found the director', Expdir, '::::', dirname)
        #     break

