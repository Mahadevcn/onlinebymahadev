import time


from selenium import webdriver
from selenium.webdriver.support.select import Select


def setup():
    print('Inside the setup() method')
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print('inside teardown() method')
    time.sleep(3)
    # driver.quit()

def test_JQiframe():
    driver.get('https://jqueryui.com/autocomplete/')
    iframe = driver.find_element_by_class_name("demo-frame")
    driver.switch_to.frame(iframe)
    tags = driver.find_element_by_xpath("//*[@id=\"tags\"]").send_keys("J")
    Objects = Select(tags)
    listoptions = Objects.options
    for opt in listoptions:
        print(opt)
