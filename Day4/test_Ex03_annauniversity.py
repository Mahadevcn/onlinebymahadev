 # from selenium import webdriver
from selenium import webdriver
from selenium.webdriver import ActionChains


def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()
    driver.get("https://www.annauniv.edu/department/index.php")
    driver.maximize_window()
    driver.implicitly_wait(10)


def teardown():
    print('Inside teardown() method')
    driver.quit()


def test_validate_iom():
    print('inside test_validate_iom() test method ')
    action = ActionChains(driver)
    menu = driver.find_element_by_xpath("//*[@id=\"link3\"]/strong")
    second = driver.find_element_by_xpath("//*[@id=\"menuItemHilite32\"]")
    action.move_to_element(menu).move_to_element(second).click().perform()
    #action.move_to_element(menu)

    print(driver.title)
