import time

import pytest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select


def setup():
    print('Inside Setup() method ')
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)


def teardown():
    print('Inside teardown() method')
    time.sleep(3)
    driver.quit()


def dataGenerator():
    lst = [['Raazi', 'Meghna Gulzar', 'Vicky'],['Baazigar','Mastan','Sha Rukh Khan']]
    return lst

@pytest.mark.parametrize('data', dataGenerator())

def test_search(data):
    movie = data[0]
    expDir = data[1]
    expStar = data[2]

    print(data)

    driver.get('https://www.imdb.com/')
    search = driver.find_element_by_xpath("//*[@ id='suggestion-search']")
    search.send_keys(movie)
    search.send_keys(Keys.ENTER)

    Movielnk = driver.find_element_by_xpath("//*[@id='main']/div/div[2]/table/tbody/tr[1]/td[2]/a")
    Movielnk.click()
    lststars = driver.find_elements_by_xpath("// *[ @class = 'inline' and contains(text(), 'Star')] / following-sibling::*")

    # // *[ @class = 'inline' and contains(text(), 'Star')] / following-sibling::*

    # // *[ @class ='inline' and contains(text(), 'Director')

    StarFound = False
    for star in lststars:
        starname = star.text
        print(starname)
        if (expStar in starname):
            starFound = True
            print('Found the star', expStar, ':::::', starname)
            break

    # // *[ @ id = "main"] / div / div[2] / table / tbody / tr[1] / td[2] / a
    # // *[ @ id = "suggestion-search"]

    # https: // www.imdb.com /
