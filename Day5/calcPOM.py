

class ataCalc:

    def __init__(self, driverobj):
        self.driver = driverobj




        self.input1 = self.driver.find_element_by_xpath("// *[ @ id = \"ID_nameField1\"]")
        self.input2 = self.driver.find_element_by_xpath("//*[@id=\"ID_nameField2\"]")

        self.mulradio = self.driver.find_element_by_xpath("//*[@id=\"gwt-uid-2\"]")

        self.addradio = self.driver.find_element_by_xpath("//*[@id=\"gwt-uid-1\"]")
        self.compradio = self.driver.find_element_by_xpath("//*[@id=\"gwt-uid-4\"]")

        self.euclidradio = self.driver.find_element_by_xpath(" // *[ @ id = \"gwt-uid-6\"]")

        # // *[ @ id = "gwt-uid-6"]

        self.calc = self.driver.find_element_by_xpath("//*[@id=\"ID_calculator\"]")
        self.resultBox = self.driver.find_element_by_xpath("//*[@id=\"ID_nameField3\"]")

    def mul(self, num1, num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))

        self.input2.clear()
        self.input2.send_keys(str(num2))

        self.mulradio.click()
        self.calc.click()

        actres = int(self.resultBox.get_attribute('value'))
        return actres


    def add(self, num1, num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))

        self.input2.clear()
        self.input2.send_keys(str(num2))

        self.addradio.click()
        self.calc.click()

        actres = int(self.resultBox.get_attribute('value'))
        return actres

    def comp(self, num1, num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))

        self.input2.clear()
        self.input2.send_keys(str(num2))

        self.compradio.click()
        self.calc.click()

        actres = int(self.resultBox.get_attribute('value'))
        return actres

    def euclid(self, num1, num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))

        self.input2.clear()
        self.input2.send_keys(str(num2))

        self.euclidradio.click()
        self.calc.click()

        actres = int(self.resultBox.get_attribute('value'))
        return actres


