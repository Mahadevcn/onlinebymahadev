import time

import pytest
from selenium import webdriver

from Day5.calcPOM import ataCalc


def setup():
    print('Inside Setup() method ')
    global driver
    driver = webdriver.Chrome()
    driver.get("http://ata123456789123456789.appspot.com/")
    driver.maximize_window()
    driver.implicitly_wait(10)


def teardown():
    print('Inside teardown() method')
    time.sleep(3)
    driver.quit()


def dataprovider():
    print('inside data provider method')
    lol = [ [12,13,'add',25],
            [10,20,'Mul',200],
            [18,32,'add',50],
            [500,100,'comp',500],
            [50,12,'add',62]   ]
    return lol



# @pytest.mark.parameterize('inputdata',dataprovider())
@pytest.mark.parametrize('inputdata', dataprovider())

def test_calc_all(inputdata):
    print(inputdata)
    input1 = inputdata[0]
    input2 = inputdata[1]
    operation = inputdata[2]
    expres = inputdata[3]
    actres = ''
    atacalcpage = ataCalc(driver)


    if(operation.upper() == 'ADD'):
        actres = atacalcpage.add(input1, input2)
    elif (operation.upper() == 'MUL'):
        actres = atacalcpage.mul(input1, input2)
    elif (operation.upper() == 'COMP'):
        actres = atacalcpage.comp(input1, input2)
    else:
        print('invalid data')

    print(actres, expres)
    assert expres == actres









# def test_calc_mul():
#     n1 = 12
#     n2 = 15
#     expres = 180
#     print(n1, ' ', n2,'  ', expres)
#     driver.get("http://ata123456789123456789.appspot.com/")
#
#     atacalcpage = ataCalc(driver)
#     actres = atacalcpage.mul(n1,n2)
#     print(actres)
#
#     assert actres == expres
#
# def test_calc_add():
#     n1 = 12
#     n2 = 15
#     expres = 27
#     print(n1, ' ', n2,'  ', expres)
#     driver.get("http://ata123456789123456789.appspot.com/")
#
#     atacalcpage = ataCalc(driver)
#     actres = atacalcpage.add(n1,n2)
#     print(actres)
#
#     assert actres == expres