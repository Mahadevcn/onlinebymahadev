import time

from selenium import webdriver


def setup():
    print('Inside Setup() method ')
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)


def teardown():
    print('Inside teardown() method')
    time.sleep(3)
    driver.quit()

def test_calc_mul():
    driver.get("http://ata123456789123456789.appspot.com/")
    num1 = 10
    num2 = 20
    expres = 200

    # // *[ @ id = "ID_nameField1"]
    input1 = driver.find_element_by_xpath("//*[@id=\"ID_nameField1\"]")
    input1.clear()
    input1.send_keys(str(num1))
    # // *[ @ id = "ID_nameField2"]
    input2 = driver.find_element_by_xpath("//*[@id=\"ID_nameField2\"]")
    input2.clear()
    input2.send_keys(str(num2))

    # // *[ @ id = "gwt-uid-2"]
    mulradio = driver.find_element_by_xpath("//*[@id=\"gwt-uid-2\"]")
    mulradio.click()
    # // *[ @ id = "ID_calculator"]  calculate button
    calc = driver.find_element_by_xpath("//*[@id=\"ID_calculator\"]")
    calc.click()

    # // *[ @ id = "ID_nameField3"]

    resultBox = driver.find_element_by_xpath("//*[@id=\"ID_nameField3\"]")
    actres = int(resultBox.get_attribute('value'))
    print('actRes :', actres, 'expres :', expres)

    assert expres == actres



def test_calc_add():
    driver.get("http://ata123456789123456789.appspot.com/")
    num1 = 12
    num2 = 30
    expres = 42

    # // *[ @ id = "ID_nameField1"]
    input1 = driver.find_element_by_xpath("//*[@id=\"ID_nameField1\"]")
    input1.clear()
    input1.send_keys(str(num1))
    # // *[ @ id = "ID_nameField2"]
    input2 = driver.find_element_by_xpath("//*[@id=\"ID_nameField2\"]")
    input2.clear()
    input2.send_keys(str(num2))


    mulradio = driver.find_element_by_xpath("//*[@id=\"gwt-uid-1\"]")
    mulradio.click()
    # //*[@id="gwt-uid-1"]  calculate button
    calc = driver.find_element_by_xpath("//*[@id=\"ID_calculator\"]")
    calc.click()

    # // *[ @ id = "ID_nameField3"]

    resultBox = driver.find_element_by_xpath("//*[@id=\"ID_nameField3\"]")
    actres = int(resultBox.get_attribute('value'))
    print('actRes :', actres, 'expres :', expres)

    assert expres == actres











