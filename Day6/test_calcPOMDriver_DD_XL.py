import time
import pandas as pd
import openpyxl
import pytest
from selenium import webdriver
from Day5.calcPOM import ataCalc


def setup():
    print('Inside Setup() method ')
    global driver
    driver = webdriver.Chrome()
    driver.get("http://ata123456789123456789.appspot.com/")
    driver.maximize_window()
    driver.implicitly_wait(10)


def teardown():
    print('Inside teardown() method')
    time.sleep(3)
    driver.quit()

def dp_pandas():
    xlpath = "C:\\Users\\Anyone\\PycharmProjects\\MyfirstPycharm project\\resource\\data\\atacalcdata.xlsx"
    df = pd.read_excel(xlpath, 'calcdata')
    lol = df.values.tolist()
    return lol


def dataprovider():
    print('inside data provider method')
    # lol = [ [12,13,'add',25],
    #         [10,20,'Mul',200],
    #         [18,32,'add',50],
    #         [500,100,'comp',500],
    #         [50,12,'add',62]   ]

    # xlpath = "C:/Users/Anyone/PycharmProjects/MyfirstPycharm project/resource/data/atacalcdata.xlsx"
    xlpath = "C:\\Users\\Anyone\\PycharmProjects\\MyfirstPycharm project\\resource\\data\\atacalcdata.xlsx"
    wb = openpyxl.load_workbook(xlpath)
    sh = wb['calcdata']
    nrows = sh.max_row
    print('no. of row:', nrows)

    lol = []

    for i in range(2, nrows+1):
        lst =[]
        v1 = sh.cell(i, 1)
        v2 = sh.cell(i, 2)
        v3 = sh.cell(i, 3)
        v4 = sh.cell(i, 4)
        lst.insert(0, v1.value)
        lst.insert(1, v2.value)
        lst.insert(2, v3.value)
        lst.insert(3, v4.value)
        lol.insert(i-1, lst)

    return lol



# @pytest.mark.parametrize('inputdata',dataprovider())

@pytest.mark.parametrize('inputdata',dp_pandas())

def test_calc_all(inputdata):
    print(inputdata)
    input1 = inputdata[0]
    input2 = inputdata[1]
    operation = inputdata[2]
    expres = inputdata[3]
    actres = ''
    atacalcpage = ataCalc(driver)


    if(operation.upper() == 'ADD'):
        actres = atacalcpage.add(input1, input2)
    elif (operation.upper() == 'MUL'):
        actres = atacalcpage.mul(input1, input2)
    elif (operation.upper() == 'COMP'):
        actres = atacalcpage.comp(input1, input2)
    elif (operation.upper() == 'EUCLID(+)'):
        actres = atacalcpage.euclid(input1,input2)
    else:
        print('invalid data')

    print(actres, expres)
    assert expres == actres









# def test_calc_mul():
#     n1 = 12
#     n2 = 15
#     expres = 180
#     print(n1, ' ', n2,'  ', expres)
#     driver.get("http://ata123456789123456789.appspot.com/")
#
#     atacalcpage = ataCalc(driver)
#     actres = atacalcpage.mul(n1,n2)
#     print(actres)
#
#     assert actres == expres
#
# def test_calc_add():
#     n1 = 12
#     n2 = 15
#     expres = 27
#     print(n1, ' ', n2,'  ', expres)
#     driver.get("http://ata123456789123456789.appspot.com/")
#
#     atacalcpage = ataCalc(driver)
#     actres = atacalcpage.add(n1,n2)
#     print(actres)
#
#     assert actres == expres